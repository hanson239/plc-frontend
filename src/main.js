// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Cookies from 'js-cookie'
import ElementUI from 'element-ui'
import App from './App'
import store from './store'
import router from './router'
import i18n from './lang' // Internationalization
import '@/icons' // icon
import '@/permission' // permission control
import * as filters from './filters' // global filters
import './styles/index.scss' // global css
import VueSocketIOExt from 'vue-socket.io-extended'
import SocketIO from 'socket.io-client'

Vue.config.productionTip = false

Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value),
})
window.moment = require('moment');
Vue.use(require('vue-moment'));

import elTableInfiniteScroll from 'el-table-infinite-scroll';
Vue.use(elTableInfiniteScroll);

let options = {
  query: {
    token: store.getters.token,
    type: 'user'
  }
};
Vue.use(VueSocketIOExt, SocketIO(process.env.VUE_APP_BASE_SOCKET, options));
// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  components: { App },
  template: '<App/>'
})

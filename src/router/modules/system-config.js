/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/config',
  component: Layout,
  redirect: '/config/cabinet',
  name: 'config',
  meta: {
    title: 'Cấu hình',
    icon: 'el-icon-setting'
  },
  children: [
    {
      path: 'cabinet',
      component: () => import('@/views/config/cabinets'),
      name: 'cabinetConfig',
      meta: { title: 'Quản lý tủ điện' }
    },
    {
      path: 'alert',
      component: () => import('@/views/dashboard/index'),
      name: 'alertConfig',
      meta: { title: 'Thiết lập cảnh báo' }
    }
  ],
};

export default routes;

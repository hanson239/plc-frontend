import Layout from '@/layout';

const routes = {
    path: '/report',
    component: Layout,
    redirect: 'report/energy',
    meta: {
        title: 'Báo cáo',
        icon: 'el-icon-pie-chart',
        permissions: ['/report']
    },
    children: [
        {
            path: 'energy',
            component: () => import('@/views/report/energy'),
            name: 'energyReport',
            meta: { title: 'Báo cáo năng lượng ', noCache: false, permissions: ['/report']},
        },
        {
            path: 'environment',
            component: () => import('@/views/report/environment'),
            name: 'environmentReport',
            meta: { title: 'Báo cáo môi trường tủ', noCache: false, permissions: ['/report']},
        },
        {
            path: 'accident',
            component: () => import('@/views/report/error'),
            name: 'troubleReport',
            meta: { title: 'Báo cáo sự cố', noCache: false, permissions: ['/report']},
        },
        {
            path: 'access',
            component: () => import('@/views/report/access'),
            name: 'accessReport',
            meta: { title: 'Báo cáo truy cập', noCache: false, permissions: ['/report']},
        },
    ],
};

export default routes;

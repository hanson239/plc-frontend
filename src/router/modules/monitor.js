import Layout from '@/layout';

const routes = {
    path: '/monitor',
    component: Layout,
    redirect: 'monitor/map',
    meta: {
        title: 'Giám sát',
        icon: 'el-icon-video-camera',
        permissions: ['/report']
    },
    children: [
        {
            path: 'map',
            component: () => import('@/views/monitor/map'),
            name: 'map',
            meta: {title: 'Bản đồ số', noCache: false, permissions: ['/report']},
        },
        {
            path: 'cabinet',
            component: () => import('@/views/monitor/cabinets'),
            name: 'cabinet',
            meta: {title: 'Bàn điều khiển', noCache: false, permissions: ['/report']},
        }
    ],
};

export default routes;

module.exports = {

  listSizeImage: [
    {
      id: '1024_768',
      name: '1024x768 (4:3)'
    },
    {
      id: '1024_576',
      name: '1024x576 (16:9)'
    },
    {
      id: '1280_960',
      name: '1280x960 (4:3)'
    },
    {
      id: '1280_720',
      name: '1280x720 (16:9)'
    },
  ],

  // location default for map
  map_location_default: {
    longitude: 105.776,
    latitude: 19.807
  },
};
